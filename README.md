# Standart deviation calculator #

This is a simple script to calculate mean value and standard deviation of measurement sequence. Result is rounded to one significant digit of deviation.

Requirements:

    sudo pip3 install uncertainties

Usage example:

    $ ./stdev.py 10.2 10.5 9.9
    Mean value with deviation: 10.2+/-0.3