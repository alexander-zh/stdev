#! /usr/bin/env python3

import sys
from statistics import mean, stdev
from uncertainties import ufloat

def exit_help():
    print('Calculate mean value and standard deviation of measurement sequence.')
    print('Usage: stdev number1 [number2 [...]]')
    sys.exit(1)

vals = sys.argv[1:]
if len(vals) == 0:
    exit_help()
try:
    vals = [float(x) for x in vals]
except ValueError:
    exit_help()
average = ufloat(mean(vals), stdev(vals))
print('Mean value with deviation: {:.1u}'.format(average))
